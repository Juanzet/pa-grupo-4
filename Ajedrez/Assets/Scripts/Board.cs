using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Board : MonoBehaviour
{
    public Transform board;

    void Start()
    {
        InstantiateBoard();
    }

    void InstantiateBoard()
    {
        for (float y = 4; y > -4; y -= 1f)
        {
            for (float x = -4; x < 4; x += 1f)
            {
                Instantiate(board, new Vector3(x, 0, y), board.rotation);
            }
        }
    }
 
}
